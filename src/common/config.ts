export const API_URL = process.env.REACT_APP_API_URL;
export const API_KEY = process.env.REACT_APP_API_KEY;
export const API_IMAGE_ORIGINAL = process.env.REACT_APP_API_IMAGE_ORIGINAL;
export const API_IMAGE_LARGE = process.env.REACT_APP_API_IMAGE_LARGE;