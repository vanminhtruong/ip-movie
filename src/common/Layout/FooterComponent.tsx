import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';
import clsx from 'clsx';
import Text from 'src/helpers/Text';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import GoogleIcon from '@mui/icons-material/Google';
import GitHubIcon from '@mui/icons-material/GitHub';
import { appStore, chPlay } from 'src/utils/imageAssets';
import { COLORS } from 'src/utils/colors';

interface FooterComponentProps {
    disable: boolean;
}

const FooterComponent = (
    props: FooterComponentProps & WithStyles<typeof styles>
) => {
    const { classes, disable } = props;

    return (
        <React.Fragment>
            <Box className={clsx(classes.Footer, 'footer')}>
                <Box className="terms-of-use"
                    color={COLORS.bodyText}>
                    <Box className="terms-title flex gap-2 !font-bold mb-4">
                        <Box component={'a'}
                            href={'/'}>
                            Terms of Use
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            Privacy Policy
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            FAQ
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            Watch List
                        </Box>
                    </Box>
                    <Text>
                        © 2021 ELotus. All Rights Reserved. All videos and shows on this
                        platform are trademarks of, and all related images and content are
                        the property of, Truongvip Inc. Duplication and copy of this is
                        strictly prohibited. All rights reserved.
                    </Text>
                </Box>
                <Box className="flow-us">
                    <Text className='!font-bold !mb-4'>
                        Flow Us:
                    </Text>
                    <Box className="flow-us-icon flex gap-3"
                        color={COLORS.bodyText}>
                        <Box component={'a'}
                            href={'/'}>
                            <FacebookIcon/>
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            <TwitterIcon/>
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            <GoogleIcon/>
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            <GitHubIcon/>
                        </Box>
                    </Box>
                </Box>
                <Box className="app-dowload ">
                    <Text className="app-dow-title !font-bold !mb-4">
                        Streamit App
                    </Text>
                    <Box className='flex gap-4'>
                        <Box component={'a'}
                            href={'/'}>
                            <img src={chPlay} 
                                alt="Ch-Play" />
                        </Box>
                        <Box component={'a'}
                            href={'/'}>
                            <img src={appStore} 
                                alt="AppStore" />
                        </Box>
                    </Box>
                   
                </Box>
            </Box>
        </React.Fragment>
    );
};
export default withStyles(styles)(FooterComponent);
