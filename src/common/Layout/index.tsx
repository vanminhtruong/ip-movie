import _ from 'lodash';
import * as React from 'react';
import {Outlet} from 'react-router-dom';
import Loading from 'src/helpers/Loading';
import FooterComponent from './FooterComponent';
import HeaderComponent from './HeaderComponent';

const Layout = () => {
    const [menuActive, setMenuActive] = React.useState(1);
    const [search, setSearch] = React.useState<string>('');
    const [activeMenu, setActiveMenu] = React.useState<boolean>(false);
    const [disableFooter, setDisableFooter] = React.useState<boolean>(false);
    
    React.useEffect(()=> {
        if(window.location.pathname === '/') {
            setMenuActive(1);
        } else if(window.location.pathname === '/now-playing') {
            setMenuActive(2);
        } else if(window.location.pathname === '/top-rated') {
            setMenuActive(3);
        }
    },[]);
    
    const handleSearch = (e: any)=> {
        if(!_.isEmpty(e))
            setSearch(e.target.value);
        else  setSearch('');
    };

    const handleOpenMenuMobile = (status: boolean) => {
        setActiveMenu(status);
    };

    const handleChangeActiveMenu = (id: number) => {
        setMenuActive(id);
    };
    return (
        <>
            <Loading>
                <HeaderComponent search={search}
                    handleOpenMenuMobile={handleOpenMenuMobile}
                    activeMenu={activeMenu}
                    handleChangeActiveMenu={handleChangeActiveMenu}
                    menuActive={menuActive}
                    handleSearch={handleSearch}/>
                <div className="container content-page">
                    <Outlet/>
                </div>
                <FooterComponent disable={disableFooter}/>
            </Loading>
            
        </>
    );
};

export default Layout;