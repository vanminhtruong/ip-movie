import HomeIcon from '@mui/icons-material/Home';
import LiveTvIcon from '@mui/icons-material/LiveTv';
import TopicIcon from '@mui/icons-material/Topic';
import { AppBar, Avatar, Badge, Box, Divider, Toolbar, Tooltip, Menu, IconButton, MenuItem, Typography } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import clsx from 'clsx';
import { isEmpty, uniqueId } from 'lodash';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InputCustom from 'src/helpers/Input';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import { logo, user } from 'src/utils/imageAssets';
import styles from './styles';
import { fetchListKeyWord } from '../../redux/actions/search';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import SearchIcon from '@mui/icons-material/Search';

interface HeaderComponentProps {
    open?: boolean,
    search?: string,
    handleSearch?: (e: any)=> void,
    handleCloseDrawer?: any,
    scrollPage?: boolean,
    handleOpenMenuMobile: (status: boolean) => void,
    activeMenu: boolean,
    handleChangeActiveMenu: (id: number)=> void,
    menuActive: number,
}

const HeaderComponent = (props: HeaderComponentProps & WithStyles<typeof styles>) => {
    const {
        classes,
        handleChangeActiveMenu,
        menuActive
    } = props;

    const [search, setSearch] = React.useState('');
    const [lastScroll, setLastScoll] = React.useState(0);
    const [menuShow, setMenuShow] = React.useState('show');
    const [searchArea, setSearchArea] = React.useState(false);
    const [anchorElUser, setAnchorElUser] = React.useState(null);


    const handleScroll = () => {
        if(window.scrollY > 200){
            if(window.scrollY < lastScroll){
                setMenuShow('show');
            }else{
                setMenuShow('hide');
            }
        }else{
            setMenuShow('top');
        }
        setLastScoll(scrollY);
    };

    React.useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [lastScroll]);


    const menuList: {
        id: number,
        icon: any,
        title: string,
        url: string,
    }[] = [
        {
            id: 1,
            icon: null,
            title: 'HOME',
            url: '/',
        },
        {
            id: 2,
            icon: null,
            title: 'MOVIE',
            url: '/movie',
        },
        {
            id: 3,
            icon: null,
            title: 'TV SHOWS',
            url: '/tv-shows',
        },
        {
            id: 4,
            icon: null,
            title: 'VIDEOS',
            url: '/videos',
        },
        {
            id: 5,
            icon: null,
            title: 'BLOG',
            url: '/blog',
        },
        {
            id: 3,
            icon: null,
            title: 'PAGES',
            url: '/pages',
        },
    ];
    const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
    const dispatch = useDispatch();
    React.useEffect(()=> {
        if(search){
            setTimeout(()=> {
                const params = {
                    query: search,
                };
                dispatch(fetchListKeyWord(params));
            }, 300 );
        }
    }, [search]);

    const handleChangeTheme = (e: any) => {
        setDark();
    };

    const setDark = () => {
        localStorage.setItem('theme', 'dark');
        document.documentElement.setAttribute('data-theme', 'dark');
    };

    const handleSearchArea = (e: any) => {
        setSearchArea(true);
    };
    const handleSearchKeyWord = (e: any) => {
        setSearch(e.target.value?.trim());
    };
    const handleOpenUserMenu1 = (event: any) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseUserMenu = (e: any) => {
        setAnchorElUser(null);
    };

    const listSearchKeyword = useSelector((state: any) => state.search.listSearchKeyword);
    React.useEffect(() => {
        if(!isEmpty(listSearchKeyword)) {
            setSearch('');
        }
    }, [listSearchKeyword]);
    
  
    return (
        <Box className={classes.root}>
            <AppBar
                color={'primary'}
                className={clsx(classes.appBarStyle, menuShow)}
            >
                <div className={clsx('container')}>
                    <Toolbar
                        className={clsx(classes.toolbarStyle, 'w-full pl-8 pr-8')}
                        disableGutters
                    >
                        <Box display={'flex'}
                            className='w-full'
                            justifyContent='space-between'
                            alignItems={'center'}>
                            
                            <Box component={'a'}
                                href= {'/'}>
                                <img src={logo}
                                    className={classes.logo}
                                    alt="" />
                            </Box>
                            
                            
                            <Box component={'ul'}
                                marginX={4}
                                className={classes.listMenu}>
                                {!isEmpty(menuList) && menuList.map(li => (
                                    <Box display={'flex'}
                                        key={uniqueId()}
                                        marginRight={3}
                                        className={clsx({'active': li.id === menuActive})}
                                        onClick={()=> handleChangeActiveMenu(li.id)}
                                        component={'li'}>
                                        <Box component='a'
                                            display={'flex'}
                                            alignItems='center'
                                            href={li.url}>
                                            <Text bodySB1> 
                                                {li.icon} 
                                                {li.title}</Text>
                                        </Box>
                                    </Box>
                                ))}
                            </Box>

                            <Box className="search-noty-user flex items-center gap-6">

                                <Box className="change-theme"
                                    onClick={handleChangeTheme} >
                                    <DarkModeIcon/>
                                </Box>
                                <Box className="box-search">
                                    <SearchIcon onClick={handleSearchArea}
                                        style={{fontSize: 28, color: COLORS.bodyText}}
                                        className='cursor-pointer'/>
                                    {searchArea? 
                                        <Box>
                                            <InputCustom className='input-search'
                                                onChange={handleSearchKeyWord}
                                                placeholder='Search for a movie, tv show,...'/>
                                            <Box className="content-search">
                                                <Box className="mb-3"><Text baseSB1
                                                    color={COLORS.dark}>Movie Search:</Text>
                                                </Box>
                                                <Divider/>
                                                {!isEmpty(listSearchKeyword) ? 
                                                    listSearchKeyword.slice(0, 4).map( (item: any) => (
                                                        <Box component={'a'}
                                                            display="flex"
                                                            key={uniqueId()}
                                                            alignItems={'center'}
                                                            href={`/movie/${item.id}-${item.name}`}>
                                                            <Text base1
                                                                color={COLORS.grayDark}>
                                                                {item?.name || ''}
                                                            </Text>
                                                        </Box>
                                                    )):
                                                    <Box className="not-found">
                                                        <Text color={COLORS.dark}>Not Found</Text>
                                                    </Box>
                                                }
                                            </Box>
                                        </Box>
                                        :
                                        null
                                    }
                                    
                                    
                                </Box>
                                <Box className="notityfication cursor-pointer">
                                    <Badge badgeContent={4} 
                                        color='error'>
                                        <NotificationsNoneIcon style={{fontSize: 28, color: COLORS.bodyText}}/>
                                    </Badge>
                                </Box>
                                <Box className="user">
                                    <IconButton onClick={handleOpenUserMenu1}>
                                        <Avatar alt="User" 
                                            src={user}/>
                                    </IconButton>
                                    <Menu
                                        sx={{ mt: '45px' }}
                                        id="menu-appbar"
                                        anchorEl={anchorElUser}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={Boolean(anchorElUser)}
                                        onClose={handleCloseUserMenu}
                                    >
                                        {settings.map((setting) => (
                                            <MenuItem key={setting}
                                                onClick={handleCloseUserMenu}>
                                                <Typography textAlign="center">{setting}</Typography>
                                            </MenuItem>
                                        ))}
                                    </Menu>
                                </Box>
                            </Box>


                        </Box>
                        
                    </Toolbar>
                </div>
            </AppBar>
        </Box>
    );
};
export default withStyles(styles)(HeaderComponent);