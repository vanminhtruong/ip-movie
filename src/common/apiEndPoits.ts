import { API_URL } from './config';

export const API_LIST_MOVIE_TRENDING = API_URL + '/trending/movie/week';
export const API_LIST_MOVIE_GENRE = API_URL + '/genre/movie/list';
export const API_MOVIE_DETAIL = API_URL + '/movie';
export const API_MOVIE_NOW_PLAYING = API_URL + '/movie/now_playing';
export const API_MOVIE_TOP_RATED = API_URL + '/movie/top_rated';
export const API_MOVIE_TRENDING_WEEK = API_URL + '/trending/all/week';
export const API_MOVIE_UPCOMING = API_URL + '/movie/upcoming';
export const API_TV_MOVIE_LASTER = API_URL + '/discover/movie';
export const API_SEARCH_KEYWORD = API_URL + '/search/keyword';