import {createTheme, responsiveFontSizes} from '@mui/material/styles';
import { COLORS } from 'src/utils/colors';
const theme: any = createTheme({
    typography: {
        fontFamily: ['inter', 'sans-serif'].join(','),
    },
    palette: {
        

    },
    components: {
        MuiCssBaseline: {
      
        },
        MuiAppBar: {
            styleOverrides: {
                colorPrimary: {
                    // backgroundColor: COLORS.white,
                }
            }
        },
        MuiTooltip: {
            styleOverrides: {
                tooltip: {
                    backgroundColor: COLORS.red,
                    '&:before': {
                        backgroundColor: COLORS.red,
                    },
                }
            }
        }
    },

    
    
});

export default responsiveFontSizes(theme);