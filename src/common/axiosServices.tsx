import axios from 'axios';
import { isElement, isEmpty } from 'lodash';
import queryString from 'query-string';
import { GET } from 'src/utils/constants';
import { API_KEY } from './config';

interface IAxios {
    url: string, 
    method?: string, 
    body?: object, 
    headers?: object, 
    delay?: number, 
    params?: object
}

const axiosServices =  (props: IAxios) => {
    const { 
        url, 
        method = GET, 
        body = null, 
        headers = null, 
        delay = 0, 
        params
    } = props;
    let uri = `${url}?api_key=${API_KEY}`;
    if(method === GET && !isEmpty(params)) {
        uri = `${url}?api_key=${API_KEY}&${queryString.stringify(params)}`;
    }
    return axios[method](uri, JSON.parse(headers), JSON.parse(body))
        .then((res: any) => {
            return res;
        })
        .catch((err: any) => {
            return err;
        });
};

export default axiosServices;