import { toast } from 'react-toastify';

export const toNonAccentVietnamese = (str: string) => {
    /* https://gist.github.com/jarvisluong/f01e108e963092336f04c4b7dd6f7e45 */
    str = str.replace(/A|Á|À|Ã|Ạ|Â|Ấ|Ầ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẵ|Ặ/g, 'A');
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/E|É|È|Ẽ|Ẹ|Ê|Ế|Ề|Ễ|Ệ/, 'E');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/I|Í|Ì|Ĩ|Ị/g, 'I');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/O|Ó|Ò|Õ|Ọ|Ô|Ố|Ồ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ỡ|Ợ/g, 'O');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/U|Ú|Ù|Ũ|Ụ|Ư|Ứ|Ừ|Ữ|Ự/g, 'U');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/Y|Ý|Ỳ|Ỹ|Ỵ/g, 'Y');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/Đ/g, 'D');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); 
    str = str.replace(/\u02C6|\u0306|\u031B/g, ''); 
    str = str.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    str = str.replace(/\s/g, '-');
    return str;
};

export const convertDataMovieToURL = (data: {id: number, title?: string, name?: string}, path: string)=> {
    return `/${path}/${data?.id}-${toNonAccentVietnamese(data?.title || data?.name || '')}`;
};

export const findIdMovie = () => {
    const pathname = window.location.pathname;
    const id = pathname.slice(pathname.indexOf('/', 2) +1, pathname.indexOf('-') -1);
    return id; 
};

export const showError = (messageErr: string) => {
    const isOnline = navigator.onLine;
    
    const message = !isOnline ? 'Lost connection': messageErr || 'An error has occurred';
    toast.error(message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
};
