/* METHOD */
export const GET = 'get';
export const POST = 'post';
export const DELETE = 'delete';
export const PUT = 'put';

/* Status code */
export const STATUS_CODES = {
    success: 200,
    failed: 400,
};

export const MODAL = {
    modalQuickView: 'modal_quickView',
};

/* Change view */
export const LIST = 'list';
export const CARD = 'card';