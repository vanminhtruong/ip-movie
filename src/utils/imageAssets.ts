import logo from 'src/assets/images/logo.png';
import chPlay from 'src/assets/images/01.jpg';
import appStore from 'src/assets/images/02.jpg';
import user from 'src/assets/images/user.jpg';
import loadingImg from 'src/assets/images/loading.gif';

export {
    logo,
    chPlay,
    appStore,
    user,
    loadingImg
};