import {
    call,
    delay, put,
    takeLatest
} from 'redux-saga/effects';
import { STATUS_CODES } from 'src/utils/constants';
import { hideLoading, showLoading, openModalSuccess } from '../actions/global';
import { fetchListTVShowFailed, fetchListTVShowSuccess } from '../actions/tvShow';
import { fetchListTVShowAPI } from '../apis/tvShow';
import { FETCH_LIST_TV_POPULAR } from '../constants/tvShow';
import { OPEN_MODAL } from '../constants/global';

interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionOpenModal(action: actionType): any {
    yield delay(200);
    const { params } = action.payload;
    yield put(openModalSuccess(params));
}

export function *WatchGlobal() {
    yield takeLatest(OPEN_MODAL, actionOpenModal);
}