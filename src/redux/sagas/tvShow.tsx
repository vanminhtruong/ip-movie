import {
    call,
    delay, put,
    takeLatest
} from 'redux-saga/effects';
import { STATUS_CODES } from 'src/utils/constants';
import { hideLoading, showLoading } from '../actions/global';
import { fetchListTVShowFailed, fetchListTVShowSuccess } from '../actions/tvShow';
import { fetchListTVShowAPI } from '../apis/tvShow';
import { FETCH_LIST_TV_POPULAR } from '../constants/tvShow';

interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionFetchListTVShowType(action: actionType): any {
    try {
	    yield put(showLoading());
	    yield delay(500);
        const { params } = action.payload;
        const response = yield call(fetchListTVShowAPI, params); 
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
		    yield put(fetchListTVShowSuccess(data));
	    } else {
        	yield put(fetchListTVShowFailed(response));
        }
	    yield put(hideLoading());
    } catch (error) {
        yield put(fetchListTVShowFailed(error));
    }
}

export function *WatchTVShow() {
    yield takeLatest(FETCH_LIST_TV_POPULAR, actionFetchListTVShowType);
}