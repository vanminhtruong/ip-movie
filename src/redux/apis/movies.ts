import { API_MOVIE_DETAIL, API_TV_MOVIE_LASTER, API_MOVIE_TOP_RATED, API_MOVIE_NOW_PLAYING } from './../../common/apiEndPoits';
import queryString from 'query-string';
import { API_LIST_MOVIE_GENRE, API_LIST_MOVIE_TRENDING } from 'src/common/apiEndPoits';
import axiosServices from 'src/common/axiosServices';
import { GET } from 'src/utils/constants';

export const fetchListMovieGenreAPI = (params = {}) => {
    let queryParams = '';
    if (Object.keys(params).length > 0) {
        try {
            queryParams = `?${queryString.stringify(params)}`;
        } catch (err) {
            queryParams = '';
        }
    }
    
    const uri = `${API_LIST_MOVIE_GENRE}${queryParams}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};

export const fetchListMovieTrendingWeekAPI = (params = {}) => {
    let queryParams = '';
    if (Object.keys(params).length > 0) {
        try {
            queryParams = `?${queryString.stringify(params)}`;
        } catch (err) {
            queryParams = '';
        }
    }
  
    const uri = `${API_TV_MOVIE_LASTER}${queryParams}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};


export const fetchListMovieDetailAPI = (id = null) => {
    const uri = `${API_MOVIE_DETAIL}/${id}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};
export const fetchListMovieTopRatedAPI = (params = {}) => {
    let queryParams = '';
    if (Object.keys(params).length > 0) {
        try {
            queryParams = `?${queryString.stringify(params)}`;
        } catch (err) {
            queryParams = '';
        }
    }
  
    const uri = `${API_MOVIE_TOP_RATED}${queryParams}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};
export const fetchListMovieNowPlayingAPI = (params = {}) => {
    let queryParams = '';
    if (Object.keys(params).length > 0) {
        try {
            queryParams = `?${queryString.stringify(params)}`;
        } catch (err) {
            queryParams = '';
        }
    }
  
    const uri = `${API_MOVIE_NOW_PLAYING}${queryParams}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};