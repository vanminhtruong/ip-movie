import * as searchConstants from '../constants/search';

/* Fetch list search keyword */
export const fetchListKeyWord = (params = {}) => ({
    type: searchConstants.FETCH_LIST_SEARCH_KEYWORD,
    payload: {
        params,
    },
});
export const fetchListKeyWordSuccess = (data: any) => ({
    type: searchConstants.FETCH_LIST_SEARCH_KEYWORD_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListKeyWordFailed = (error: any) => ({
    type: searchConstants.FETCH_LIST_SEARCH_KEYWORD_FAILED,
    payload: {
        error,
    },
});
