import * as tvShowContants from '../constants/tvShow';

/* Fetch list tv popular */
export const fetchListTVShow = (params = {}) => ({
    type: tvShowContants.FETCH_LIST_TV_POPULAR,
    payload: {
        params,
    },
});
export const fetchListTVShowSuccess = (data: any) => ({
    type: tvShowContants.FETCH_LIST_TV_POPULAR_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListTVShowFailed = (error: any) => ({
    type: tvShowContants.FETCH_LIST_TV_POPULAR_FAILED,
    payload: {
        error,
    },
});
