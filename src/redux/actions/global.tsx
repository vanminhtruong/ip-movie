import * as globalConstants from '../constants/global';

export const showLoading = () => ({
    type: globalConstants.SHOW_LOADING,
});

export const hideLoading = () => ({
    type: globalConstants.HIDE_LOADING,
});

export const changeLanguage = () => ({
    type: globalConstants.CHANGE_LANGUAGE,
});

export const openModal = (title: string, data: any) => {
    const params = {
        title,
        data
    };
    return ({
        type: globalConstants.OPEN_MODAL,
        payload: {
            params,
        },
    });
};
export const openModalSuccess = (data: any) => ({
    type: globalConstants.OPEN_MODAL_SUCCESS,
    payload: {
        data,
    },
});
