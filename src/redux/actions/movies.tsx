import * as movieConstants from '../constants/movies';

/* Fetch list movie genre */
export const fetchListMovieGenre = (params = {}) => ({
    type: movieConstants.FETCH_LIST_MOVIE_GENRE,
    payload: {
        params,
    },
});
export const fetchListMovieGenreSuccess = (data: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_GENRE_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListMovieGenreFailed = (error: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_GENRE_FAILED,
    payload: {
        error,
    },
});

/* Fetch list movie trending of week */
export const fetchListMovieTrendingWeek = (params = {}) => ({
    type: movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK,
    payload: {
        params,
    },
});
export const fetchListMovieTrendingWeekSuccess = (data: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListMovieTrendingWeekFailed = (error: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK_FAILED,
    payload: {
        error,
    },
});
/* Fetch list movie detail */
export const fetchMovieDetail = (params = {}) => ({
    type: movieConstants.FETCH_LIST_MOVIE_DETAIL,
    payload: {
        params,
    },
});
export const fetchMovieDetailSuccess = (data: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_DETAIL_SUCCESS,
    payload: {
        data,
    },
});
export const fetchMovieDetailFailed = (error: any) => ({
    type: movieConstants.FETCH_LIST_MOVIE_DETAIL_FAILED,
    payload: {
        error,
    },
});

export const fetchListNowPlaying = (params = {}) => ({
    type: movieConstants.FETCH_LIST_NOW_PLAYING,
    payload: {
        params,
    },
});
export const fetchListNowPlayingSuccess = (data: any) => ({
    type: movieConstants.FETCH_LIST_NOW_PLAYING_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListNowPlayingFailed = (error: any) => ({
    type: movieConstants.FETCH_LIST_NOW_PLAYING_FAILED,
    payload: {
        error,
    },
});

export const fetchListTopRated = (params = {}) => ({
    type: movieConstants.FETCH_LIST_TOP_RATED,
    payload: {
        params,
    },
});
export const fetchListTopRatedSuccess = (data: any) => ({
    type: movieConstants.FETCH_LIST_TOP_RATED_SUCCESS,
    payload: {
        data,
    },
});
export const fetchListTopRatedFailed = (error: any) => ({
    type: movieConstants.FETCH_LIST_TOP_RATED_FAILED,
    payload: {
        error,
    },
});