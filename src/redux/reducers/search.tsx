import * as searchConstants from '../constants/search';

const initialState = {
    isLoading: true,
    error: null,
    listSearchKeyword: null,
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any; }; }) => {
    switch(action.type) {
        /* Fetch list search keyword */
        case searchConstants.FETCH_LIST_SEARCH_KEYWORD: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listSearchKeyword: null
            };
        }
        case searchConstants.FETCH_LIST_SEARCH_KEYWORD_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                listSearchKeyword: data.results,
            };
        }
        case searchConstants.FETCH_LIST_SEARCH_KEYWORD_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listSearchKeyword: null,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;