import * as globalConstants from '../constants/global';

const initialState = {
    isLoading: true,
    error: null,
    modal: '',
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any; }; }) => {
    switch(action.type) {
        /* Fetch list tv show popular */
        case globalConstants.OPEN_MODAL: {
            return {
                ...state,
                isLoading: true,
                error: null,
                modal: '',
            };
        }
        case globalConstants.OPEN_MODAL_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: true,
                error: null,
                modal: data,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;