import * as tvShowContants from '../constants/tvShow';

const initialState = {
    isLoading: true,
    error: null,
    listTVShow: null,
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any; }; }) => {
    switch(action.type) {
        /* Fetch list tv show popular */
        case tvShowContants.FETCH_LIST_TV_POPULAR: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listTVShow: null
            };
        }
        case tvShowContants.FETCH_LIST_TV_POPULAR_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                listTVShow: data.results,
            };
        }
        case tvShowContants.FETCH_LIST_TV_POPULAR_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listTVShow: null,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;