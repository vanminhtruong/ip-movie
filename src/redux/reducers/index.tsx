import { combineReducers } from 'redux';
import movieReducer from './movies';
import tvShowReducer from './tvShow';
import globalReducer from './global';
import searchReducer from './search';

const rootReducer = combineReducers({
    movie: movieReducer,
    tv: tvShowReducer,
    global: globalReducer,
    search: searchReducer,
});

export default rootReducer;
