import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, uniqueId } from 'lodash';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import MovieCard from 'src/helpers/MovieCard';
import Text from 'src/helpers/Text';
import { convertDataMovieToURL } from 'src/utils';
import { COLORS } from 'src/utils/colors';
import styles from './styles';
import { IUpcomingComponent } from './types';

function UpcomingComponent(props: IUpcomingComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listMovieTrending,
        limit
    } = props;
    const navigate = useNavigate();

    return (
        <Box className={classes.movieUpcoming}>
            <Box className="title-element my-4 flex items-center justify-between">
                <Text h4
                    color={COLORS.bodyText}>Upcoming Movies</Text>
                <Box
                    component={'a'}
                    href={'/'}>
                    <Text titleSB1
                        color={COLORS.red}
                    >View All</Text>
                </Box>
            </Box>
            <Grid container
                spacing={2}>
                {!isEmpty(listMovieTrending) ? listMovieTrending.slice(0, limit).map(movie => (
                    <Grid item
                        lg={2}
                        md={4}
                        xs={6}
                        className='movie-coming-item'
                        key={movie.id}>
                        <MovieCard
                            genre={'movie'}
                            movie={movie}/>
                    </Grid>
                )):
                    new Array(12).fill(null).map(_ => (
                        <Grid item
                            lg={2}
                            md={4}
                            xs={6}
                            className='movie-coming-item'
                            key={uniqueId()}>
          
                            <Skeleton key={uniqueId()}
                                variant="rectangular"
                                width={'100%'}
                                animation="wave"
                                style={{marginRight: 10, borderRadius: '12px'}}
                                height={450} />
                        </Grid>))}
            </Grid>
        </Box>
    );
}

export default withStyles(styles)(UpcomingComponent);