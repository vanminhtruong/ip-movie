import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        
    },
    topSlider: {
        position: 'relative',

        '& .movie-item': {
            position: 'relative',
            borderRadius: '0px',
            overflow: 'hidden',
            '& img': {
                width: '100%',
                height: 'calc(100vh - 80px)',
                objectFit: 'cover',
            },
            '&__title': {
                textShadow: '-1px 0px 2px rgba(0,0,0,0.6)',
                '&:hover': {
                    color: `${COLORS.green} !important`
                }
            },
            '& .content': {
                display: 'flex',
                alignItems: 'center',
                padding: '0 100px',
                position: 'absolute',
                bottom: 0,
                left: 0,
                right: 0,
                height: 'calc(100vh - 80px)',

                '& .info-movie': {
                    width: '60%',

                },
                '& .trailer-btn': {
                    width: '40%',
                },
                '& ul.group-info': {
                    display: 'flex',
                    '& li': {
                        marginRight: 10,
                        color: COLORS.white,
                        '& svg': {
                            marginRight: 4,
                        }
                    }
                }
            }
        }
    },
    movieUpcoming: {
        marginTop: 50,
        marginBottom: 50,
        '& .movie-coming-item': {
            
        }
    }
});

export default styles;