import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { API_IMAGE_LARGE, API_IMAGE_ORIGINAL } from 'src/common/config';
import 'swiper/css';
import 'swiper/css/navigation';
import Slider from './Slider';
import styles from './styles';
import TvShow from './movieLaster';
import { IHomeComponent } from './types';
import Upcoming from './Upcoming';
import { isEmpty, round } from 'lodash';
import ModalCustom from 'src/helpers/ModalCustom';
import { MODAL } from 'src/utils/constants';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import { convertDataMovieToURL } from '../../utils/index';
import MovieLaster from './movieLaster';

function HomeComponent(props: IHomeComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listMovieGenre,
        listMovieTrending,
        listMovieLaster,
       
    } = props;
    const navigate = useNavigate();
    const LIMIT_MOVIE = 12;
    const modal = useSelector((state: any) => state.global.modal);
    const urlImage = modal.data?.poster_path ? `${API_IMAGE_LARGE}${modal.data?.poster_path}`
        : 'https://via.placeholder.com/300x400';

    return (
        <Box className={classes.root}>
            <Slider
                listMovieGenre={listMovieGenre}
                listMovieTrending={listMovieTrending}
                limit={LIMIT_MOVIE}
            />
            <Upcoming
                listMovieTrending={listMovieTrending}
                limit={LIMIT_MOVIE}
            />
            <MovieLaster
                listMovieLaster={listMovieLaster}
                limit={LIMIT_MOVIE}
            />
            {!isEmpty(modal?.data) && (
                <ModalCustom title={MODAL.modalQuickView}>
                    <img src={urlImage}
                        alt=""
                        className="bg-modal" />
                    <Grid container
                        spacing={4}>
                        <Grid item
                            xs={12}
                            md={6}>
                            <img width={300}
                                src={`${API_IMAGE_LARGE}${modal.data?.poster_path}`}
                                alt="" />
                        </Grid>
                        <Grid item
                            xs={12}
                            md={6}>
                            <Box component={'a'}
                                href={convertDataMovieToURL(modal.data,'movie')}>
                                <Text h4
                                    color={COLORS.white}>{modal.data?.title}</Text>
                            </Box>
                            <Box className="mt-2">
                                <Text color={COLORS.white}>Debut: {modal.data?.release_date}</Text>
                            </Box>
                            <Box className="mt-2">
                                <Text color={COLORS.white}>Vote: {round(modal.data?.vote_average,2)}</Text>
                            </Box>
                            <Box className="mt-2">
                                <Text color={COLORS.white}>Vote: {modal.data?.overview}</Text>
                            </Box>
                        </Grid>
                    </Grid>
                </ModalCustom>
            )}
             
        </Box>
    );
}

export default withStyles(styles)(HomeComponent);