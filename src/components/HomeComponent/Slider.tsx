import { Box, Button, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './styles';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { Autoplay, Navigation } from 'swiper';
import { ISliderComponent } from './types';
import { get, isEmpty, round, uniqueId } from 'lodash';
import { API_IMAGE_ORIGINAL } from '../../common/config';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import StarIcon from '@mui/icons-material/Star';
import { convertDataMovieToURL } from 'src/utils';
import { logo } from 'src/utils/imageAssets';

function SliderComponent(props: ISliderComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listMovieGenre,
        listMovieTrending,
        limit,
    } = props;
    const navigate = useNavigate();

    return (
        <Box className={classes.topSlider}>
            <Swiper
                spaceBetween={0}
                slidesPerView={1}
                navigation={false} 
                modules={[Navigation, Autoplay]}
                loop={true}
                autoplay={{

                    delay: 5500,
                    disableOnInteraction: false,
                }}
            >
                {!isEmpty(listMovieTrending) ? listMovieTrending.slice(0, limit).map(movie => (
                    <SwiperSlide key={movie.id}>
                        <Box className={'movie-item'}>


                            <img src={`${API_IMAGE_ORIGINAL}${movie.backdrop_path}`}
                                alt={movie.title} />


                            <Box className="content">
                                <Box className="info-movie">
                                    {/* Logo */}
                                    {/* <img src={logo}
                                        alt="logo"
                                        width={'200px'}
                                        className='img-logo' /> */}
                                    {/* Name of Film */}
                                    <a href={convertDataMovieToURL(movie, 'movie')}>
                                        <Text h2
                                            className='movie-item__title'
                                            color={COLORS.white}>{movie.title}</Text>
                                    </a>
                                    {/* Rating Film */}
                                    <Box component='ul'
                                        className="group-info">
                                        <Box display={'flex'}
                                            component='li'>
                                            <StarIcon/>
                                            <Text captionM2
                                                color={COLORS.white}>
                                                {round(movie.vote_average, 2)}
                                            </Text>
                                        </Box>
                                        <Box display={'flex'}
                                            component='li'>
                                            <Text captionM2
                                                color={COLORS.white}>
                                                {movie.release_date}
                                            </Text>
                                        </Box>
                                        {!isEmpty(movie.genre_ids) && movie.genre_ids.map(genre => {
                                            const genreName = !isEmpty(listMovieGenre) 
                                                ? listMovieGenre.find(o => o.id === genre) :'';
                                            return (
                                                <Box display={'flex'}
                                                    key={uniqueId()}
                                                    component='li'>
                                                    <Text captionM1
                                                        color={COLORS.white}>
                                                        {get(genreName, 'name', '')}
                                                    </Text>
                                                </Box>
                                            );
                                        })}
                                        
                                    </Box>
                                    {/* Decription */}
                                    <Box className="decription">

                                    </Box>
                                    {/* Gener */}
                                    <Box className="gener">

                                    </Box>
                                    {/* Button Play */}
                                    <Box className="play-btn">

                                    </Box>
                                </Box>
                                <Box className="trailer-btn">

                                </Box>
                            </Box>
                        </Box>
                    </SwiperSlide>
                )):
                    <div className="flex">
                        <Skeleton variant="rectangular"
                            style={{borderRadius: '12px'}}
                            width={'calc(100% - 25px)'}
                            animation="wave"
                            height={'calc(100vh - 80px'} />
                    </div>
                }
                {/* Button Prev and Next */}
                <Box className="prev-btn">

                </Box>
                <Box className="next-btn">

                </Box>
            </Swiper>
        </Box>
    );
}

export default withStyles(styles)(SliderComponent);