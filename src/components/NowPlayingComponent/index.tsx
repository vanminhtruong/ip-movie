import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, uniqueId } from 'lodash';
import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { API_IMAGE_LARGE } from 'src/common/config';
import MovieCard from 'src/helpers/MovieCard';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import 'swiper/css';
import 'swiper/css/navigation';
import styles from './styles';
import { INowPlayingComponent } from './types';
import ListIcon from '@mui/icons-material/List';
import { CARD, LIST } from 'src/utils/constants';
import ViewCard from './viewCard';
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import clsx from 'clsx';
import ViewList from './viewList';

function NowPlayingComponent(props: INowPlayingComponent & WithStyles<typeof styles>) {
    const {
        classes,
    } = props;
    return (
        <Box className={classes.root}>
            
        </Box>
    );
}

export default withStyles(styles)(NowPlayingComponent);