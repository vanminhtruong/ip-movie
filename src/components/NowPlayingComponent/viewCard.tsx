import { Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, uniqueId } from 'lodash';
import React from 'react';
import MovieCard from 'src/helpers/MovieCard';
import 'swiper/css';
import 'swiper/css/navigation';
import styles from './styles';
import { INowPlayingComponent } from './types';

function ViewCard(props: INowPlayingComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listNowPlaying,
        handleChangeView,
        view,
    } = props;
    return (

        <Grid container
            spacing={2}>
            {!isEmpty(listNowPlaying) ? listNowPlaying.slice(0, 18).map(movie => (
                <Grid item
                    lg={2}
                    md={4}
                    xs={6}
                    className='movie-coming-item'
                    key={movie.id}>
                    <MovieCard
                        genre={'movie'}
                        movie={movie}/>
                </Grid>
            )):
                new Array(18).fill(null).map(_ => (
                    <Grid item
                        lg={2}
                        md={4}
                        xs={6}
                        className='movie-coming-item'
                        key={uniqueId()}>
          
                        <Skeleton key={uniqueId()}
                            variant="rectangular"
                            width={'100%'}
                            animation="wave"
                            style={{marginRight: 10,borderRadius: '12px'}}
                            height={450} />
                    </Grid>))}
        </Grid>
    );
}

export default withStyles(styles)(ViewCard);