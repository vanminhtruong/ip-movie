import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, round, uniqueId } from 'lodash';
import React from 'react';
import MovieCard from 'src/helpers/MovieCard';
import Text from 'src/helpers/Text';
import TooltipCustom from 'src/helpers/TooltipCustom';
import styles from './styles';
import { INowPlayingComponent } from './types';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { COLORS } from 'src/utils/colors';
import { API_IMAGE_LARGE } from 'src/common/config';
import { convertDataMovieToURL } from 'src/utils';

function ViewList(props: INowPlayingComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listNowPlaying,
        view,
    } = props;
    return (
        <Box className='list-movie'>
            {!isEmpty(listNowPlaying) ? listNowPlaying.slice(0, 18).map(movie => (
                <Box display={'flex'}
                    className='item'
                    marginBottom={4 }
                    alignItems={'center'}
                    key={uniqueId()}>
                    <img src={`${API_IMAGE_LARGE}${movie.poster_path}`}
                        alt="" />
                    <Box className="content">
                        <Box marginLeft={4}>
                            <Box
                                component={'a'}
                                href={convertDataMovieToURL(movie, 'movie')}
                            >
                                <Text h4
                                    color={COLORS.black}
                                    className="title">
                                    {movie?.title || movie?.name || ''}
                                </Text>
                            </Box>
                            <Box className="mt-2">
                                <Text color={COLORS.black}>Debut: {movie?.release_date}</Text>
                            </Box>
                            <Box className="mt-2">
                                <Text color={COLORS.black}>Vote: {round(movie?.vote_average,2)}</Text>
                            </Box>
                            <Box className="mt-2 overview">
                                <Text color={COLORS.black}>Vote: {movie?.overview}</Text>
                            </Box>
                        </Box>
                    </Box>
                    
                </Box>
            )):
                new Array(18).fill(null).map(_ => (
                    <Skeleton key={uniqueId()}
                        variant="rectangular"
                        width={'100%'}
                        animation="wave"
                        style={{marginRight: 10,borderRadius: '12px'}}
                        height={450} />
                ))}
        </Box>
    );
}

export default withStyles(styles)(ViewList);