export interface IMovieDetail {
    listMovieGenre?: {
        id: number,
        name: string,
    }[],
    data: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
        overview: string,
    },
    limit?: number,
}