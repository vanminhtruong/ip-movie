import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { round, uniqueId, isEmpty } from 'lodash';
import React from 'react';
import { API_IMAGE_LARGE, API_IMAGE_ORIGINAL } from 'src/common/config';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import { convertDataMovieToURL } from '../../utils/index';
import styles from './styles';
import { IMovieDetail } from './types';

function MovieDetailComponent(props: IMovieDetail & WithStyles<typeof styles>) {
    const {
        classes,
        data,
    } = props;


    const urlImage = data?.poster_path ? `${API_IMAGE_LARGE}${data?.poster_path}`
        : 'https://via.placeholder.com/300x400';

    return (
        <Box className={classes.root}
            alignItems='center'>
            <Grid container
                spacing={4}>
                <Grid item
                    xs={12}
                    md={6}>
                    {!isEmpty(data) ?  <img width={300}
                        src={urlImage}
                        alt="" />:
                        <Skeleton key={uniqueId()}
                            variant="rectangular"
                            width={300}
                            animation="wave"
                            style={{marginRight: 10,borderRadius: '12px'}}
                            height={450} />
                    }
                   
                 
                </Grid>
                <Grid item
                    display='flex'
                    justifyContent={'center'}
                    flexDirection='column'
                    xs={12}
                    md={6}>
                    {!isEmpty(data) ? <>
                        <Box component={'a'}
                            href={convertDataMovieToURL(data,'movie')}>
                            <Text h4
                                color={COLORS.dark}>{data?.title}</Text>
                        </Box>
                        <Box className="mt-2">
                            <Text color={COLORS.dark}>Debut: {data?.release_date}</Text>
                        </Box>
                        <Box className="mt-2">
                            <Text color={COLORS.dark}>Vote: {round(data?.vote_average,2)}</Text>
                        </Box>
                        <Box className="mt-2">
                            <Text color={COLORS.dark}>Vote: {data?.overview}</Text>
                        </Box></>:
                        <>
                            <Skeleton key={uniqueId()}
                                variant="rectangular"
                                animation="wave"
                                width={300}
                                style={{marginRight: 10, marginBottom: 30, borderRadius: '12px'}}
                                height={30} />
                            <Skeleton key={uniqueId()}
                                variant="rectangular"
                                animation="wave"
                                width={400}
                                style={{marginRight: 10, marginBottom: 30, borderRadius: '12px'}}
                                height={30} />
                            <Skeleton key={uniqueId()}
                                variant="rectangular"
                                animation="wave"
                                width={450}
                                style={{marginRight: 10, marginBottom: 30, borderRadius: '12px'}}
                                height={70} />
                        </>
                    }
                </Grid>
            </Grid>
        </Box>
    );
}

export default withStyles(styles)(MovieDetailComponent);