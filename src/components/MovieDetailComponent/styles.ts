import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        position: 'relative',
        maxWidth: 992,
        padding: 15,
        margin: '0 auto',
        '& img.bg-modal': {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            filter: 'brightness(.6)',
            zIndex: -1,
        },

        '& .box-title': {
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 20,
            position: 'fixed',
            top: 30,
            right: 30,
            height: 36,
            zIndex: 2,
            '& .title': {
      
            },
            '& svg': {
                color: COLORS.white,
                width: 30,
                height: 30,
            },
            '& .box-close': {

            },
            '& .icon-btn-closed': {
                width: 36,
                height: 36,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: COLORS.grey3,
                borderRadius: '50%',
                transition: 'all .3s ease',
                boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.06)',
                '& .icon-svg': {
                    width: 20,
                    height: 20,
                    transition: 'all .3s ease',
                },
                '&:hover': {
                    '& .icon-svg': {
                        transform: 'rotate(90deg)',
                    }
                },
                '&.white': {
                    backgroundColor: COLORS.white,
                }
            }
        },
    },
});

export default styles;