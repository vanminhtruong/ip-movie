import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, uniqueId } from 'lodash';
import React from 'react';
import MovieCard from 'src/helpers/MovieCard';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import 'swiper/css';
import 'swiper/css/navigation';
import styles from './styles';
import { ITopReadComponent } from './types';
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import ListIcon from '@mui/icons-material/List';
import clsx from 'clsx';
import { CARD, LIST } from 'src/utils/constants';
import ViewList from './viewList';
import ViewCard from './viewCard';

function TopReadComponent(props: ITopReadComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listMovieTopRated,
        handleChangeView,
        view,
    } = props;
    return (
        <Box className={classes.root}>
            <Box className={classes.movieUpcoming}>
                <div className="flex items-center justify-between my-4">
                    <Box className="title-element">
                        <Text h4
                            color={COLORS.black}>Top Rated</Text>
                    </Box>
                    <Box className="change-view">
                        <Box>
                            <ViewModuleIcon className={clsx({'active': view === CARD})}
                                onClick={()=> handleChangeView(CARD)}/>
                            <ListIcon className={clsx({'active': view === LIST})}
                                onClick={()=> handleChangeView(LIST)}/>
                        </Box>
                    </Box>
                </div>
                {view === CARD && (
                    <ViewCard
                        listMovieTopRated={listMovieTopRated}
                    />
                )} 
                {view === LIST && (
                    <ViewList
                        listMovieTopRated={listMovieTopRated}
                    />
                )} 
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TopReadComponent);