import { Tooltip } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import styles from './styles';

interface IMenuCustom {
    title: string,
    children: any,
    className?: string,
}

const MenuCustom: React.FC = (props: IMenuCustom & WithStyles<typeof styles>) => {
    const 
        { classes,
            title,
            children,
            className,
            ...otherProps
        } = props;

    return (
        <>
            
        </>

    );
};
export default withStyles(styles)(MenuCustom);