import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {

        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        
        '& img': {
            width: '20%',
        }
        
    }

});

export default styles;