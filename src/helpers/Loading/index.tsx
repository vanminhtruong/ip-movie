import { Box, Tooltip } from '@mui/material';
import React, { useState } from 'react';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import { COLORS } from 'src/utils/colors';
import styles from './styles';
import { loadingImg } from 'src/utils/imageAssets';

interface ILoading {
    time: number,
    children: React.ReactNode,
}

const Loading: React.FC = (props: ILoading & WithStyles<typeof styles>) => {
    const { classes,
        time = 1000,
        children,
    } = props;

    const [loading ,setLoading] = useState<boolean>(true);



    React.useEffect(() => {
        const loadingTimeOut = setTimeout(() => {
            setLoading(false);
        },time);
        return () => {
            clearTimeout(loadingTimeOut);
        };
    },[loading]);

    return (
        <Box>
            {loading?
                <Box className={classes.root}>
                    <img src={loadingImg} 
                        alt={'Loading'}/>
                </Box>
                :
                <Box>
                    {children}
                </Box> 
            }
        </Box>
    );
};
export default withStyles(styles)(Loading);