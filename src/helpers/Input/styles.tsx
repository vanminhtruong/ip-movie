import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    rootInput: {
        width: 350,
        transition: 'all 0.5s ease',
        '& svg': {
            cursor: 'pointer'
        },

    },
    notification: {
        color: COLORS.bg1,
        width: 24,
        height: 24,
        borderRadius: 6,
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center',
    },
    searchBox: {
        backgroundColor: COLORS.bg1,
        borderRadius: '12px',
        color: COLORS.shades40,
        position: 'relative',
        height: 48,
        display: 'flex',
        alignItems: 'center',
        marginBottom: '0 !important',
    },
    iconSearch: {
        position: 'absolute',
        left: 8,
        top: 12,
        bottom: 12,
    },
    appBarStyle: {
        width: 'calc(100% - 340px) !important',
        boxShadow: 'none !important',
        padding: 20
    },
    inputSearch: {
        '& input': {
            fontWeight: 'bold',
            padding: '10px 25px 10px 35px',
            border: '2px solid transparent',
            borderRadius: 12,
            '&:focus': {
                outline: 'none',
                border: `2px solid  ${COLORS.blue}`,
            },

        },
        '&.error input': {
            outline: 'none',
            border: `2px solid  ${COLORS.red} !important`,
        }

    },
    commandF: {
        padding: '4px 12px',
        borderRadius: 8,
        backgroundColor: COLORS.white,
        position: 'absolute',
        top: 8,
        color: COLORS.black,
        bottom: 8,
        cursor: 'pointer',
        right: 12,
        //width: 56,
        // color: COLORS.black,
        fontWeight: 'bold',
        transition: 'all .3s ease',
        width:'max-content',
        '&:hover': {
            color: COLORS.blueHover,
        }
    },
    boxClosed: {
        width: 30,
        height: 30,
        // position: 'absolute !important',
        top: 8,
        bottom: 8,
        right: 8,
        backgroundColor: `${COLORS.bg1} !important`,
        '&:hover .icon-svg': {
            color: COLORS.red,
        }
    },
    price: {
        position: 'relative',
        width: '100%',
        '& > div:first-of-type': {
            overflow: 'hidden',
            position: 'absolute !important',
            width: '100%',
            border: '2px solid #EFEFEF',
            borderRadius:'12px',
        },
        '& input': {
            background: '#FFFFFF !important',
            paddingLeft: '58px !important',
        },
        '& .box-icon':{
            position:'absolute',
            width: '50px',
            height: '50px',
            background: `${COLORS.bg1} !important`,
            borderTopLeftRadius: '12px',
            borderBottomLeftRadius: '12px',
            display:'flex',
            alignItems:'center',
            justifyContent:'center'
        },

    },
});

export default styles;