import { Box, IconButton, Input, SxProps } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import * as React from 'react';
import { COLORS } from 'src/utils/colors';
import Text from '../Text';
import styles from './styles';
import SearchIcon from '@mui/icons-material/Search';

interface IInputCustom {
    placeholder?: string;
    isFocused?: boolean;
    disableIconSearch?: false ;
    onBlur?: ()=> void;
    onChange?: (e: React.MouseEvent<HTMLInputElement>)=> void;
    onfocus?: ()=> void;
    commandF?: boolean;
    className?: string;
    style?: any;
    search?: string,
    handleSearch?: (e: React.MouseEvent<HTMLInputElement>)=> void;
    sx?: SxProps;
    icon?: string;
    type?: string;
    name?: string;
    autoComplete?: string;
    defaultValue?: string;
    messangeError?: string;
}

const InputCustom = React.forwardRef((props: IInputCustom & WithStyles<typeof styles>,
    ref: any) => {
    const {classes, isFocused, placeholder, commandF, search, style
        , handleSearch,disableIconSearch,className, icon, type, messangeError, ...otherProps} = props;
       
    return (
        <Box component={'div'}
            className={classes.rootInput}>
            <Box
                className={clsx(
                    className && className,
                    classes.searchBox,
                    'input'
                )}
                style={style}
            >
                <Input
                    type={type || 'text'}
                    autoFocus={isFocused}
                    fullWidth
                    placeholder={placeholder}
                    disableUnderline
                    ref={ref} 
                    className={clsx(classes.inputSearch, {['error']: Boolean(messangeError)})}
                    {...otherProps}
                />
                {
                    !disableIconSearch && <Box
                        component={'div'}
                        className={classes.iconSearch}
                    >
                        <SearchIcon style={{color: COLORS.bodyText ,width: 24, height: 24}}/>
                    </Box>
                }
            </Box>
            {Boolean(messangeError) && <Text caption2
                color={COLORS.red}>{messangeError}</Text>}
        </Box>
    );
});

export default withStyles(styles)(InputCustom);