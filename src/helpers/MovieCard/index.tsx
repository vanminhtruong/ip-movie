import { Box, Grid, Tooltip } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import { API_IMAGE_LARGE, API_IMAGE_ORIGINAL } from '../../common/config';
import styles from './styles';
import { IMovieCardComponent } from './types';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import VisibilityIcon from '@mui/icons-material/Visibility';
import TooltipCustom from '../TooltipCustom';
import ModalCustom from '../ModalCustom';
import { MODAL } from 'src/utils/constants';
import { useDispatch } from 'react-redux';
import { openModal } from '../../redux/actions/global';
import { convertDataMovieToURL } from 'src/utils';

function MovieCardComponent(props: IMovieCardComponent & WithStyles<typeof styles>) {
    const {
        classes,
        movie,
        genre,
    } = props;
    const navigate = useNavigate();
    const dispatch = useDispatch();

    return (
        <Box className={classes.root}>
            <Box className="movie-item"  >
                <img src={`${API_IMAGE_LARGE}${movie.poster_path}`}
                    alt="" />


                <Box className="content-container">
                    <Box className={'content'}>
                        <Text titleM1
                            color={COLORS.bodyText}
                            className="title">
                            {movie?.title || movie?.name || ''}
                        </Text>
                        <Text captionM1 
                            color={COLORS.bodyText}
                            className={'!mt-1'}
                        >
                            2hr:22mins
                        </Text>
                    </Box>
                </Box>


                <Box className="group-action">
                    <TooltipCustom title='Quick View'>
                        <Box className='cursor-pointer item'
                            onClick={() => {
                                dispatch(openModal(MODAL.modalQuickView, movie));
                            }}
                        >
                            <VisibilityIcon />
                        </Box>
                    </TooltipCustom>
                    {/* <TooltipCustom title={'Add To Wishlist'}>
                        <FavoriteBorderIcon/>
                    </TooltipCustom> */}
                </Box>
            </Box>
           
        </Box>
    );
}

export default withStyles(styles)(MovieCardComponent);