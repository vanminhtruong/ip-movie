import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        '& .movie-item': {
            position: 'relative',
            overflow: 'hidden',
            borderRadius: '4px',
            '& img': {
                position: 'relative',
                width: '100%',
                overflow: 'hidden',
                borderRadius: '4px',
                transition: 'all .2s ease',
            },
            '& .content-container': {
                marginTop: 15,
                marginBottom: 15,
                // position: 'absolute',
                // zIndex: '10000000 !important',

                '& .content': {
                    

                },

                '& .text': {
                    width: '85%',
                    color: COLORS.white,
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                },
                '& .title': {
                    
                    '&:hover': {
                        color: `${COLORS.red} !important`,
                    }
                }
            },
            '& .group-action': {
                display: 'flex',
                position: 'absolute',
                bottom: '40%',
                left: 'calc(50% - 20px)',
                borderRadius: '4px',
                justifyContent: 'center',
                padding: '2px 4px',
                background: '#ffffff4a',
                transition: 'all .2s ease',
                opacity: 0,
                visibility: 'hidden',
                '& svg': {
                    width: 25,
                    color: COLORS.red,
                },
                '& svg:hover': {
                    color: COLORS.orangeLight,
                },
            },
            '&:hover': {
                '& img': {
                    opacity: 0.9
                },
                '& .group-action': {
                    transform: 'translateY(-15px)',
                    opacity: 1,
                    visibility: 'visible',
                   
                }
            }
        }
    },
});

export default styles;