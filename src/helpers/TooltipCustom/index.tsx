import { Tooltip } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { COLORS } from 'src/utils/colors';
import styles from './styles';

interface ITooltipCustom {
    title: string,
    children: any,
    className?: string,
}

const TooltipCustom: React.FC = (props: ITooltipCustom & WithStyles<typeof styles>) => {
    const { classes, title, children, className, ...otherProps } = props;

    return (
        <>
            <Tooltip arrow
                placement={'top'}
                className={clsx('icon-tooltip', className)}
                classes={{ arrow: classes.arrow, tooltip: classes.tooltip }}
                title={title}>
                {children}
            </Tooltip>
        </>

    );
};
export default withStyles(styles)(TooltipCustom);