import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    arrow: {
        '&:before': {
            color: COLORS.red,
        }
    },
    tooltip: {

        backgroundColor: `${COLORS.red} !important`,
        color: COLORS.red,
    },

});

export default styles;