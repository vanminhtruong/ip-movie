import * as React from 'react';
import { SxProps, TextField, Typography  } from '@mui/material';
import clsx from 'clsx';
import { COLORS } from 'src/utils/colors';

interface TypographyProps {
    children: React.ReactNode | string,
    h1?: boolean,
    h2?: boolean,
    h3?: boolean, 
    h4?: boolean,
    titleSB1?: boolean,
    titleM1?: boolean,
    baseSB1?: boolean,
    baseB1?: boolean,
    base1?: boolean,
    base2?: boolean,
    bodyM1?: boolean,
    bodySB1?: boolean,
    bodySB2?: boolean,
    caption1?: boolean,
    caption2?: boolean,
    captionM1?: boolean,
    captionM2?: boolean,
    button1?: boolean,
    button2?: boolean,
    titleMobile?: boolean,
    className?: string,
    style?: object,
    color?: string,
    center?: boolean,
    sx?: SxProps,
    component?: string,
    paddingY?: number,
}

const Text = (props: TypographyProps) => {
    const { 
        children,
        h1,
        h2,
        h3, 
        h4,
        titleSB1,
        titleM1,
        baseSB1,
        baseB1,
        base1,
        base2,
        bodyM1,
        bodySB1,
        bodySB2,
        caption1,
        caption2,
        captionM1,
        captionM2,
        button1,
        button2,
        titleMobile,
        className,
        style,
        color = COLORS.bodyText,
        center,
        sx,
        paddingY,
        ...otherProps
    } = props;


    const styles = {
        h1: {
            fontSize: 64,
            fontWeight: 600,
            '@media(max-width: 800px)': {
                fontSize: 40,
            }
        },
        h2: {
            fontSize: 48,
            fontWeight: 600,
            '@media(max-width: 800px)': {
                fontSize: 40,
            }
        },
        h3: {
            fontSize: 40,
            fontWeight: 600,
            '@media(max-width: 800px)': {
                fontSize: 30,
            },
        },
        h4: {
            fontSize: 32,
            fontWeight: 600,
            '@media(max-width: 800px)': {
                fontSize: 22,
            }
        },
        titleSB1: {
            fontSize: 20,
            fontWeight: 600,
            '@media(max-width: 800px)': {
                fontSize: 16,
            }
        },
        titleM1: {
            fontSize: 20,
            fontWeight: 500,
            '@media(max-width: 800px)': {
                fontSize: 18,
            }
        },
        baseSB1: {
            fontSize: 15,
            fontWeight: 600,
        },
        baseB1: {
            fontSize: 15,
            fontWeight: 700,
        },
        base1: {
            fontSize: 14,
            fontWeight: 600,
        },
        base2: {
            fontSize: 14,
            lineHeight: '24px',
            fontWeight: 600,
        },
        bodyM1: {
            fontSize: 15,
            fontWeight: 500,
        },
        bodySB1: {
            fontSize: 15,
            fontWeight: 600,
        },
        bodySB2: {
            fontSize: 14,
            fontWeight: 600,
        },
        caption1: {
            fontSize: 13,
            fontWeight: 600,
        },
        caption2: {
            fontSize: 12,
            fontWeight: 700,
        },
        captionM1: {
            fontSize: 13,
            fontWeight: 500,
        },
        captionM2: {
            fontSize: 12,
            fontWeight: 500,
        },
        button1: {
            fontSize: 15,
            fontWeight: 700,
        },
        button2: {
            fontSize: 13,
            fontWeight: 700,
        },
        titleMobile: {
            fontSize: 18,
            fontWeight: 600,
        }
    };

    return (
        <Typography
            style={{
                ...style, 
                color: color || 'currentColor',
                textAlign: center ? 'center' : 'start',
                transition: 'all .3s ease',
                
            }}
            
            sx={{
                ...h1 && styles.h1,
                ...h2 && styles.h2,
                ...h3 && styles.h3,
                ...h4 && styles.h4,
                ...bodyM1 && styles.bodyM1,
                ...bodySB1 && styles.bodySB1,
                ...bodySB2 && styles.bodySB2,
                ...titleM1 && styles.titleM1,
                ...titleSB1 && styles.titleSB1,
                ...base1 && styles.base1, 
                ...base2 && styles.base2, 
                ...baseB1 && styles.baseB1,
                ...baseSB1 && styles.baseSB1,
                ...caption1 && styles.caption1,
                ...caption2 && styles.caption2,
                ...captionM1 && styles.captionM1,
                ...captionM2 && styles.captionM2,
                ...button1 && styles.button1,
                ...button2 && styles.button2,
                ...titleMobile && styles.titleMobile,
                ...sx,
                ...paddingY && {paddingLeft: paddingY, paddingRight: paddingY},
            }}
            className={clsx(className && className, 'text')}
            {...otherProps}
        >
            {children}
        </Typography>
    );
};

export default Text;