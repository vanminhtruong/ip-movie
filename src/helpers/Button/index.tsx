import { Button, ButtonProps, SxProps } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import Text from '../Text';
import styles from './styles';

enum ButtonTypes {
    'button',
    'submit',
    'reset',
    undefined
} 

interface ButtonCustomProps {
    children: React.ReactNode,
    iconBefore?: any,
    iconAfter?: any,
    isIconBefore?: boolean,
    isIconAfter?: boolean,
    color?: string,
    className?: any,
    sx?: SxProps,
    onClick?: any,
    disabled?: boolean,
    isElement?: React.ReactNode,
}


const ButtonCustom = React.forwardRef((props: ButtonCustomProps & ButtonProps & WithStyles<typeof styles>, ref) => {
    const {classes,
        children,
        iconBefore,
        iconAfter,
        isIconBefore,
        isIconAfter,
        color ='primary',
        className,
        isElement,
        ...otherProps} = props;
    return (
        <React.Fragment>
            <Button
                ref={ref || React.useRef() as any}
                className={clsx(
                    classes.buttonCustom,
                    'button-custom',
                    {
                        [classes.colorPrimary]: color === 'primary',
                        [classes.colorDefault]: color === 'default',
                        [classes.colorTransparent]: color === 'transparent',
                    },
                    className && className,
                )}
                
                style={{
                    textTransform: 'capitalize',

                }}
                {...otherProps}
            >
                {isElement ? children: 
                    <>
                        {
                            isIconBefore && (
                                <img src={iconBefore}
                                    width={20}
                                    height={20}
                                    className={classes.iconBefore}

                                />
                            )
                        }
                        <Text button1>
                            {children}
                        </Text>
                        {
                            isIconAfter && (
                                <img src={iconAfter}
                                    width={20}
                                    height={20}
                                    className={classes.iconBefore}

                                />
                            )
                        }
                    </>}
               
            </Button>
        </React.Fragment>
    );
});

export default withStyles(styles)(ButtonCustom);