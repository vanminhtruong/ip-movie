import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import * as movieActions from 'src/redux/actions/movies';
import * as tvShowActions from 'src/redux/actions/tvShow';
import HomeComponent from 'src/components/HomeComponent';

const HomeContainer = () => {
    const dispatch = useDispatch();
    const listMovieGenre = useSelector((state: any) => state.movie.listMovieGenre);
    const listMovieTrending = useSelector((state: any) => state.movie.listMovieTrending);
    const listMovieLaster = useSelector((state: any) => state.tv.listTVShow);
    
    useEffect(()=> {
        dispatch(movieActions.fetchListMovieGenre());
        dispatch(movieActions.fetchListMovieTrendingWeek());
        dispatch(tvShowActions.fetchListTVShow());
    }, []);

    return (
        <HomeComponent
            listMovieGenre={listMovieGenre}
            listMovieTrending={listMovieTrending}
            listMovieLaster={listMovieLaster}
        />
    );
};

export default HomeContainer; 