import React, { useEffect } from 'react';
import '../assets/scss/global.scss';
import '../assets/scss/custom.scss';
import {BrowserRouter as Router, Routes} from 'react-router-dom';
import {ThemeProvider} from '@mui/material/styles';
import Routers from 'src/routers';
import { CssBaseline } from '@mui/material';
import theme from 'src/common/theme';
import {ToastContainer, Slide} from 'react-toastify';
import configureStore from 'src/redux/configureStore';
import { Provider } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';

const store = configureStore();


function App() {
    return (
        <ThemeProvider theme={theme}>
            <Provider store={store}>
                <ToastContainer
                    position="top-center"
                    limit={1}
                    autoClose={50000}
                    transition={Slide}
                    closeButton={true}
                />
                <CssBaseline />
                <Router>
                    <Routers/>
                </Router>
            </Provider>
        </ThemeProvider>
    );
}

export default App;
