import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MovieDetailComponent from 'src/components/MovieDetailComponent';
import * as movieActions from 'src/redux/actions/movies';
import * as tvShowActions from 'src/redux/actions/tvShow';
import { findIdMovie, showError } from '../../utils/index';
import { fetchMovieDetail } from '../../redux/actions/movies';
import { isEmpty } from 'lodash';
import { toast } from 'react-toastify';

const MovieDetailContainer = () => {
    const dispatch = useDispatch();
    useEffect(()=> {
        const idMovie = findIdMovie();
        dispatch(fetchMovieDetail(idMovie));
    }, []);
    const movieDetail = useSelector((state: any) => state.movie.movieDetail);
    const error = useSelector((state: any)=> state.movie.error);
    
    if(error) {
        showError(error?.message);
    }

    if(!isEmpty(movieDetail)) {
        document.title = movieDetail?.title;
    }
    return (
        <MovieDetailComponent
            data={movieDetail}/>
    );
};

export default MovieDetailContainer; 