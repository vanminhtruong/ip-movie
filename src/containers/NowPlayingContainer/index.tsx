import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import NowPlayingComponent from 'src/components/NowPlayingComponent';
import * as movieActions from 'src/redux/actions/movies';
import { CARD } from 'src/utils/constants';

const NowPlayingContainer = () => {
    const [view, setView] = useState(CARD);
    const dispatch = useDispatch();
    const listNowPlaying = useSelector((state: any) => state.movie.listMovieNowPlaying);
    
    useEffect(()=> {
        dispatch(movieActions.fetchListNowPlaying());
    }, []);

    const handleChangeView = (value: string)=> {
        setView(value);
    };

    return (
        <NowPlayingComponent
            listNowPlaying={listNowPlaying}
            handleChangeView={handleChangeView}
            view={view}
        />
    );
};

export default NowPlayingContainer; 